#ifndef GAME_STATE_HPP
#define GAME_STATE_HPP

#include "../Kuko/states/IState.hpp"
#include "../Kuko/managers/MenuManager.hpp"

#include "../entities/Map.hpp"
#include "../entities/Player.hpp"

#include <vector>
#include <string>

class GameState : public kuko::IState
{
    public:
    GameState( const std::string& contentPath );
    virtual ~GameState();

    virtual void Setup();
    virtual void Cleanup();
    virtual void Update();
    virtual void Draw();

    void DrawHud();

    virtual void SetContentPath( const std::string& path );

    private:
    Map m_map;
    Player m_player;

    kuko::UILabel m_lblRemainingDebt;
    kuko::UILabel m_remainingDebt;
    kuko::UILabel m_lblCurrentDelivery;
    kuko::UILabel m_currentDelivery;
    kuko::Sprite m_playerFace;
    std::vector<kuko::Sprite> m_itemSlots;

    kuko::IntRect m_screenOffset;

    std::string m_state;

    std::string m_contentPath;
};

#endif
