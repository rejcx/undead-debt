#include "GameState.hpp"

#include "../Kuko/managers/ImageManager.hpp"
#include "../Kuko/managers/FontManager.hpp"
#include "../Kuko/managers/InputManager.hpp"
#include "../Kuko/base/Application.hpp"
#include "../Kuko/utilities/StringUtil.hpp"
#include "../Kuko/utilities/Logger.hpp"

GameState::GameState( const std::string& contentPath )
{
}

GameState::~GameState()
{
}

void GameState::Setup()
{
    m_map.Generate();
    m_player.Setup( 550, 2200 );

    kuko::ImageManager::AddTexture( "bullet", "content/graphics/bullet.png" );

    int textHeight = 30;
    m_lblRemainingDebt.Setup( "m_lblRemainingDebt", "Remaining Debt", kuko::FloatRect( 100, 20, 100, textHeight ), false, { 0xff, 0xff, 0xff, 0xff }, kuko::FontManager::GetFont( "main" ) );
    m_remainingDebt.Setup( "m_remainingDebt", "$30000", kuko::FloatRect( 100, 50, 100, textHeight ), false, { 0xff, 0xff, 0xff, 0xff }, kuko::FontManager::GetFont( "main" ) );
    m_lblCurrentDelivery.Setup( "m_lblCurrentDelivery", "Current Delivery", kuko::FloatRect( 300, 20, 100, textHeight ), false, { 0xff, 0xff, 0xff, 0xff }, kuko::FontManager::GetFont( "main" ) );
    m_currentDelivery.Setup( "m_currentDelivery", "Kansas City", kuko::FloatRect( 300, 50, 100, textHeight ), false, { 0xff, 0xff, 0xff, 0xff }, kuko::FontManager::GetFont( "main" ) );

    kuko::ImageManager::AddTexture( "playerface", "content/graphics/player_face.png" );
    m_playerFace.SetTexture( kuko::ImageManager::GetTexture( "playerface" ) );
    m_playerFace.SetPosition( kuko::FloatRect( 10, 10, 70, 70 ) );

    m_screenOffset.w = kuko::Application::GetDefaultWidth();
    m_screenOffset.h = kuko::Application::GetDefaultHeight();

    // add handgun
    kuko::ImageManager::AddTexture( "handgun", "content/graphics/item_gun.png" );
    kuko::ImageManager::AddTexture( "handgun_ammo", "content/graphics/item_ammo.png" );

    kuko::Sprite handgun;
    handgun.SetTexture( kuko::ImageManager::GetTexture( "handgun" ) );
    handgun.SetPosition( kuko::FloatRect( kuko::Application::GetDefaultWidth() - 80, 10, 70, 70 ) );
    m_itemSlots.push_back( handgun );

    kuko::Sprite handgunAmmo;
    handgunAmmo.SetTexture( kuko::ImageManager::GetTexture( "handgun_ammo" ) );
    handgunAmmo.SetPosition( kuko::FloatRect( kuko::Application::GetDefaultWidth() - 80, 90, 70, 70 ) );
    m_itemSlots.push_back( handgunAmmo );

    m_player.RegisterScreenOffset( &m_screenOffset );
}

void GameState::Cleanup()
{
}

void GameState::Update()
{
    kuko::InputManager::Update();
    std::map<kuko::CommandButton, kuko::TriggerInfo> input = kuko::InputManager::GetTriggerInfo();
    std::string keyHit = kuko::InputManager::GetKeyHit();
    kuko::IntRect mousePosition = kuko::InputManager::GetMousePosition();
    m_player.UpdateCrosshairs( mousePosition.x, mousePosition.y );
    m_player.HandleMovement( input );

    // Game Offset
    m_screenOffset.x = kuko::Application::GetDefaultWidth() / 2 - ( m_player.GetPosition().x + m_player.GetPosition().w / 2 );
    m_screenOffset.y = kuko::Application::GetDefaultHeight() / 2 - ( m_player.GetPosition().y + m_player.GetPosition().h / 2 );

    m_player.Update();
}

void GameState::Draw()
{
    m_map.Draw( m_screenOffset );
    m_player.Draw();

    DrawHud();
}

void GameState::DrawHud()
{
    m_lblRemainingDebt.Draw();
    m_remainingDebt.Draw();
    m_lblCurrentDelivery.Draw();
    m_currentDelivery.Draw();
    m_playerFace.Draw();

    for ( unsigned int i = 0; i < m_itemSlots.size(); i++ )
    {
        m_itemSlots[i].Draw();
    }
    kuko::ImageManager::DrawRectangle( m_itemSlots[0].GetPosition(), 255, 255, 255, 2, false );

    // Maximum health
    kuko::ImageManager::DrawRectangle( kuko::FloatRect( 10, 90, 250, 20 ), 0, 0, 0, 1, true );
    // Current health
    kuko::ImageManager::DrawRectangle( kuko::FloatRect( 10, 90, 225, 20 ), 255, 0, 0, 1, true );

    m_map.DrawMinimap( m_player );
}

void GameState::SetContentPath( const std::string& path )
{
    m_contentPath = path;
}

