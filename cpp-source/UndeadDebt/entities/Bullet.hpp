#ifndef BULLET_HPP
#define BULLET_HPP

#include "../Kuko/managers/ImageManager.hpp"
#include "../Kuko/entities/BaseEntity.hpp"

class Bullet : public kuko::BaseEntity
{
    public:
    void Setup( int startX, int startY, float angle );
    void Draw( kuko::IntRect& screenOffset );
    void Update();

    bool GetIsDestroyed() { return m_deleteMe; }

    private:
    bool m_deleteMe;
    float m_angle;
    float m_speed;
};

#endif
