#include "Bullet.hpp"

#include "../Kuko/base/Application.hpp"

#include <cmath>

void Bullet::Setup( int startX, int startY, float angle )
{
    SetTexture( kuko::ImageManager::GetTexture( "bullet" ) );
    m_position.x = startX;
    m_position.y = startY;
    m_position.w = m_position.h = 7;
    m_angle = angle;
    m_speed = 700;
}

void Bullet::Update()
{
    // move x, y
    float xAmount = cos( m_angle );
    float yAmount = sin( m_angle );

    m_position.x += xAmount * m_speed * kuko::Application::GetTimeStep();
    m_position.y += yAmount * m_speed * kuko::Application::GetTimeStep();

    kuko::BaseEntity::UpdateSprite();
}

void Bullet::Draw( kuko::IntRect& screenOffset )
{
    m_sprite.DrawWithOffset( screenOffset );
}

