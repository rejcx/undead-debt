#include "City.hpp"

#include "../Kuko/utilities/StringUtil.hpp"

City::City()
{
}

City::City( const std::string& name, const std::string& textureName, int x, int y )
{
    Setup( name, textureName, x, y );
}


void City::Setup( const std::string& name, const std::string& textureName, int x, int y )
{
    this->name = name;
    sprite.SetTexture( kuko::ImageManager::GetTexture( textureName ) );

    int w, h;
    kuko::ImageManager::GetTextureDimensions( textureName, &w, &h );

    sprite.SetPosition( kuko::FloatRect( x, y, w, h ) );

    SDL_Texture* cityLabelTexture = kuko::ImageManager::GenerateLabelTexture( name, { 0xff, 0xff, 0xff, 0xff }, kuko::FontManager::GetFont( "main" ) );
    lblName.SetTexture( cityLabelTexture );
    lblName.SetPosition( kuko::FloatRect( x + w/2, y + h, w/2, 20 ) );
}

void City::Draw( kuko::IntRect& screenOffset )
{
    sprite.DrawWithOffset( screenOffset );
    lblName.DrawWithOffset( screenOffset );
}
