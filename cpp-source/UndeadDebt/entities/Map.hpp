#ifndef MAP_HPP
#define MAP_HPP

#include "Player.hpp"
#include "City.hpp"

#include <vector>
#include <string>

class Map
{
    public:
    void Generate();
    void Draw( kuko::IntRect& screenOffset );
    void DrawMinimap( const Player& player );

    private:
    void LoadImages();

    std::vector<kuko::Sprite> m_baseTiles;
    std::vector<kuko::Sprite> m_topTiles;

    std::vector<City> m_cities;

    int m_worldWidth, m_worldHeight;
    int m_tileWH;
};

#endif
