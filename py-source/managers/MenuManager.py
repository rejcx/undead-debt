    #!/usr/bin/python
# -*- coding: utf-8 -*-

import pygame, sys, math
from pygame.locals import *

from ezha import FontManager
from ezha import ConfigManager
from ezha import Utilities
from ezha import Menu
from ezha import Logger

class MenuManager:
    menuX = 1280 - 320
    
    @classmethod
    def IsMenuOpen( pyClass, menuRef ):
        return ( menuRef.IsOpen() )
        
        
    @classmethod
    def BuildHud( pyClass, menuRef, imageLibrary, playerRef ):
        # Hud width
        width = int(ConfigManager.options[ "screenwidth" ]) / 4
        height = int(ConfigManager.options[ "screenheight" ])
        
        hudX = 1280 - width
        
        menuRef.SetBackground( { "texture" : imageLibrary["hudbg"], "color" : pygame.Color( 0, 0, 0 ), "width" : width, "height" : height, "x" : hudX, "y" : 0 } )
        
        menuRef.AddLabel( "date", { "label_text" : "Jan 1", "label_font" : FontManager.Get( "hud_sub2" ), "label_position" : ( hudX+30, 20 ), "label_color" : pygame.Color( 255, 255, 255 ) } ) #, "label_secondary_color" : pygame.Color( 0, 0, 0 ), "label_style" : "outline" } )
        menuRef.AddImage( "heart",                 { "image_texture" : imageLibrary[ "heart" ],     "image_position" : ( hudX+120, 20 ), "image_blitrect" : ( 0, 0, 16, 16 ) } )
        menuRef.AddPercentBar( "health",           { "bar_color"  : pygame.Color( 0, 255, 0 ),    "width" : 150, "height" : 10,      "initial_value" : 100, "position" : ( hudX + 150, 20 ) } )
        
        
        menuRef.AddLabel( "money", { "label_font" : FontManager.Get( "hud_sub" ),  "label_text" : playerRef.money.GetAmountString(), "label_position" : ( hudX+30, 175 ), "label_color" : pygame.Color( 255, 255, 255 ), "label_secondary_color" : pygame.Color( 0, 0, 0 ), "label_style" : "outline" } )

        menuRef.AddLabel( "fps", { "label_text" : "FPS: " + str( int( Utilities.GetFPSClock().get_fps() ) ), "label_font" : FontManager.Get( "hud_sub2" ), "label_position" : ( hudX+50, 400 ), "label_color" : pygame.Color( 255, 255, 255 ), "label_secondary_color" : pygame.Color( 0, 0, 0 ), "label_style" : "outline" } )
        
        x = hudX + 45
        y = 270
        
        menuRef.AddImage( "inventory1", { "image_texture" : imageLibrary["items"],  "image_position" : ( x, y ), "image_blitrect" : ( 0, 0, 50, 50 ) } )
        menuRef.AddLabel( "inventory1label", { "label_text" : "Handgun", "label_font" : FontManager.Get( "hud_sub2" ), "label_position" : ( x, y+60 ), "label_color" : pygame.Color( 0, 0, 0 ) } ) #, "label_secondary_color" : pygame.Color( 0, 0, 0 ), "label_style" : "outline" } )
        
        x += 90
        
        menuRef.AddImage( "inventory2", { "image_texture" : imageLibrary["items"],  "image_position" : ( hudX+135, y ), "image_blitrect" : ( 0, 0, 50, 50 ) } )
        menuRef.AddLabel( "inventory2label", { "label_text" : "Ammo", "label_font" : FontManager.Get( "hud_sub2" ), "label_position" : ( x, y+60 ), "label_color" : pygame.Color( 0, 0, 0 ) } ) #, "label_secondary_color" : pygame.Color( 0, 0, 0 ), "label_style" : "outline" } )
        
        x += 90
        
        menuRef.AddImage( "inventory3", { "image_texture" : imageLibrary["items"],  "image_position" : ( hudX+225, y ), "image_blitrect" : ( 0, 0, 50, 50 ) } )
        menuRef.AddLabel( "inventory3label", { "label_text" : "Letter", "label_font" : FontManager.Get( "hud_sub2" ), "label_position" : ( x, y+60 ), "label_color" : pygame.Color( 0, 0, 0 ) } ) #, "label_secondary_color" : pygame.Color( 0, 0, 0 ), "label_style" : "outline" } )
        
        
        menuRef.AddLabel( "ammoCount", { "label_text" : "", "label_font" : FontManager.Get( "hud_sub" ), "label_position" : ( 1130, 100 ),  "label_color" : pygame.Color( 255, 255, 255 ), "label_secondary_color" : pygame.Color( 0, 0, 0 ), "label_style" : "outline" } )


        menuRef.AddLabel( "lblToDo", { "label_text" : "To Do", "label_font" : FontManager.Get( "hud_sub2" ), "label_position" : ( hudX+30, 65 ), "label_color" : pygame.Color( 0, 0, 0 ) } ) #, "label_secondary_color" : pygame.Color( 0, 0, 0 ), "label_style" : "outline" } )
        menuRef.AddLabel( "statusNote", { "label_text" : playerRef.status.Get(), "label_font" : FontManager.Get( "hud_sub" ), "label_position" : ( hudX+30, 100 ), "label_color" : pygame.Color( 0, 0, 0 ) } ) #, "label_secondary_color" : pygame.Color( 0, 0, 0 ), "label_style" : "outline" } )

        x = hudX + 25
        y = 530
        
        for key, loan in playerRef.loans.iteritems():
            menuRef.AddLabel( "loanHeader" + loan.GetName(), { "label_text" : loan.GetName(),            "label_position" : ( x,    y ),  "label_font" : FontManager.Get( "hud_sub2" ), "label_color" : pygame.Color( 255, 255, 255 ) } ) #, "label_secondary_color" : pygame.Color( 0, 0, 0 ), "label_style" : "outline" } )
            menuRef.AddLabel( "principle"  + loan.GetName(), { "label_text" : loan.GetPrincipleString(), "label_position" : ( x+10, y+30 ), "label_font" : FontManager.Get( "hud_sub2" ),   "label_color" : pygame.Color( 255, 255, 255 ) } ) # "label_secondary_color" : pygame.Color( 0, 0, 0 ), "label_style" : "outline" } )
            menuRef.AddLabel( "interest"   + loan.GetName(), { "label_text" : loan.GetInterestString(),  "label_position" : ( x+10, y+60 ), "label_font" : FontManager.Get( "hud_sub2" ),  "label_color" : pygame.Color( 255, 255, 255 ) } ) # "label_secondary_color" : pygame.Color( 0, 0, 0 ), "label_style" : "outline" } )

        x = hudX + 45
        y = 667
        
        menuRef.AddButton( "btnSave", {
            "button_texture" : imageLibrary["phone_icons"],
            "button_position" : ( x, y ),
            "button_blitrect" : ( 0, 0, 45, 45 ),
            "button_type" : "confirm",
            } )
        
        x += 90
        
        menuRef.AddButton( "btnOptions", {
            "button_texture" : imageLibrary["phone_icons"],
            "button_position" : ( x, y ),
            "button_blitrect" : ( 45, 0, 45, 45 ),
            "button_type" : "confirm",
            } )
        
        x += 90
        
        menuRef.AddButton( "btnPause", {
            "button_texture" : imageLibrary["phone_icons"],
            "button_position" : ( x, y ),
            "button_blitrect" : ( 90, 0, 45, 45 ),
            "button_type" : "confirm",
            } )

        menuRef.Show()

    @classmethod
    def BuildCityMenu( pyClass, menuRef, imageLibrary, cityName, playerRef ):
        Logger.Write( "Create menu", "Menus::BuildCityMenu", "#00ff00" )
        pyClass.ClearMenu( menuRef )
        menuRef.SetName( "CityMenu" )

        screenWidth = int(ConfigManager.options[ "screenwidth" ])
        screenHeight = int(ConfigManager.options[ "screenheight" ])
        width = 320
        height = 600

        menuLeft = pyClass.menuX
        menuTop = 100
        menuRight = menuLeft + width
        menuBottom = menuTop + height

        buttony = 75
        inc = 75

        menuRef.SetBackground( { "color" : pygame.Color( 50, 0, 0 ), "x" : menuLeft, "y" : menuTop, "width" : width, "height" : height } )

        menuRef.AddLabel( "cityname", {
            "label_font" : FontManager.Get( "main" ),
            "label_text" : cityName,
            "label_position" : ( menuLeft + 10, menuTop + 10 ),
            "label_color" : pygame.Color( 255, 255, 255 )
            } )

        menuRef.AddButton( "shop", {
            "button_texture" : imageLibrary["buttonbg_ingame"],
            "button_position" : ( menuLeft + 10, menuTop + buttony ),
            "button_type" : "confirm",
            "label_font" : FontManager.Get( "hud" ),
            "label_text" : "Shop",
            "label_position" : ( 30, 10 ),
            "label_color" : pygame.Color( 255, 255, 255 )#, "label_secondary_color" : pygame.Color( 0, 0, 0 ), "label_style" : "outline"
            } )

        menuRef.AddButton( "loan", {
            "button_texture" : imageLibrary["buttonbg_ingame"],
            "button_position" : ( menuLeft + 10, menuTop + buttony + (inc * 1) ),
            "button_type" : "confirm",
            "label_font" : FontManager.Get( "hud" ),
            "label_text" : "Pay loans",
            "label_position" : ( 30, 10 ),
            "label_color" : pygame.Color( 255, 255, 255 )#, "label_secondary_color" : pygame.Color( 0, 0, 0 ), "label_style" : "outline"
            } )
        
        if ( playerRef.job is not None and playerRef.job.GetDestination() == cityName and playerRef.job.GetType() == "deliver-letter" ):
            menuRef.AddButton( "deliver-letter", {
                "button_texture" : imageLibrary["buttonbg_ingame"],
                "button_position" : ( menuLeft + 10, menuTop + buttony + (inc * 2) ),
                "button_type" : "confirm",
                "label_font" : FontManager.Get( "hud" ),
                "label_text" : "Deliver letter",
                "label_position" : ( 30, 10 ),
                "label_color" : pygame.Color( 255, 255, 255 )#, "label_secondary_color" : pygame.Color( 0, 0, 0 ), "label_style" : "outline"
                } )
                
        if ( playerRef.job is not None and playerRef.job.GetDestination() == cityName and playerRef.job.GetType() == "deliver-food" ):
            menuRef.AddButton( "deliver-food", {
                "button_texture" : imageLibrary["buttonbg_ingame"],
                "button_position" : ( menuLeft + 10, menuTop + buttony + (inc * 2) ),
                "button_type" : "confirm",
                "label_font" : FontManager.Get( "hud" ),
                "label_text" : "Deliver food",
                "label_position" : ( 30, 10 ),
                "label_color" : pygame.Color( 255, 255, 255 )#, "label_secondary_color" : pygame.Color( 0, 0, 0 ), "label_style" : "outline"
                } )
            
        elif ( playerRef.job == None ):
            menuRef.AddButton( "job", {
                "button_texture" : imageLibrary["buttonbg_ingame"],
                "button_position" : ( menuLeft + 10, menuTop + buttony + (inc * 2) ),
                "button_type" : "confirm",
                "label_font" : FontManager.Get( "hud" ),
                "label_text" : "Find a job",
                "label_position" : ( 30, 10 ),
                "label_color" : pygame.Color( 255, 255, 255 )#, "label_secondary_color" : pygame.Color( 0, 0, 0 ), "label_style" : "outline"
                } )

        menuRef.AddButton( "rest", {
            "button_texture" : imageLibrary["buttonbg_ingame"],
            "button_position" : ( menuLeft + 10, menuTop + buttony + (inc * 3) ),
            "button_type" : "confirm",
            "label_font" : FontManager.Get( "hud" ),
            "label_text" : "Rest ($20)",
            "label_position" : ( 30, 10 ),
            "label_color" : pygame.Color( 255, 255, 255 )#, "label_secondary_color" : pygame.Color( 0, 0, 0 ), "label_style" : "outline"
            } )
            
        menuRef.AddButton( "leavecity", {
            "button_texture" : imageLibrary["buttonbg_ingame"],
            "button_position" : ( menuLeft + 10, menuTop + buttony + (inc * 6) ),
            "button_type" : "cancel",
            "label_font" : FontManager.Get( "hud" ),
            "label_text" : "Leave",
            "label_position" : ( 30, 10 ),
            "label_color" : pygame.Color( 255, 255, 255 )#, "label_secondary_color" : pygame.Color( 0, 0, 0 ), "label_style" : "outline"
            } )
            
        menuRef.Show()

    @classmethod
    def BuildLoanMenu( pyClass, menuRef, imageLibrary, playerRef ):
        Logger.Write( "Create menu", "Menus::BuildLoanMenu", "#00ff00" )
        pyClass.ClearMenu( menuRef )
        menuRef.SetName( "LoanMenu" )

        screenWidth = int(ConfigManager.options[ "screenwidth" ])
        screenHeight = int(ConfigManager.options[ "screenheight" ])
        width = 320
        height = 450

        menuLeft = pyClass.menuX
        menuTop = 100
        menuRight = menuLeft + width
        menuBottom = menuTop + height

        buttony = 75
        inc = 75

        menuRef.SetBackground( { "color" : pygame.Color( 0, 50, 0 ), "x" : menuLeft, "y" : menuTop, "width" : width, "height" : height } )
        
        x = menuLeft + 10
        y = menuTop
        
        for key, loan in playerRef.loans.iteritems():
            menuRef.AddLabel( "header", {
                "label_font" : FontManager.Get( "main" ),
                "label_text" : "Repay loan",
                "label_position" : ( x, y + 10 ),
                "label_color" : pygame.Color( 255, 255, 255 )
                } )

            menuRef.AddLabel( "principle", {
                "label_font" : FontManager.Get( "hud" ),
                "label_text" : "Principle: " + loan.GetPrincipleString(),
                "label_position" : ( x, y + 100 ),
                "label_color" : pygame.Color( 255, 255, 255 )
                } )

            menuRef.AddLabel( "interest", {
                "label_font" : FontManager.Get( "hud" ),
                "label_text" : "Interest: " + loan.GetInterestString(),
                "label_position" : ( x, y + 150 ),
                "label_color" : pygame.Color( 255, 255, 255 )
                } )
            
            menuRef.AddButton( "loanpay-min", {
                "button_texture" : imageLibrary["buttonbg_ingame"],
                "button_position" : ( x, menuTop + buttony + (inc * 2) ),
                "button_type" : "confirm",
                "label_font" : FontManager.Get( "hud" ),
                "label_text" : "Minimum (" + loan.GetMinimumPaymentString() + ")",
                "label_position" : ( 30, 10 ),
                "label_color" : pygame.Color( 255, 255, 255 )#, "label_secondary_color" : pygame.Color( 0, 0, 0 ), "label_style" : "outline"
                } )

            menuRef.AddButton( "loanpay-more", {
                "button_texture" : imageLibrary["buttonbg_ingame"],
                "button_position" : ( menuLeft + 10, menuTop + buttony + (inc * 3) ),
                "button_type" : "confirm",
                "label_font" : FontManager.Get( "hud" ),
                "label_text" : "Pay more",
                "label_position" : ( 30, 10 ),
                "label_color" : pygame.Color( 255, 255, 255 )#, "label_secondary_color" : pygame.Color( 0, 0, 0 ), "label_style" : "outline"
                } )

        menuRef.AddButton( "loancancel", {
            "button_texture" : imageLibrary["buttonbg_ingame"],
            "button_position" : ( menuLeft + 10, menuTop + buttony + (inc * 4) ),
            "button_type" : "cancel",
            "label_font" : FontManager.Get( "hud" ),
            "label_text" : "Cancel",
            "label_position" : ( 30, 10 ),
            "label_color" : pygame.Color( 255, 255, 255 )#, "label_secondary_color" : pygame.Color( 0, 0, 0 ), "label_style" : "outline"
            } )
            
        menuRef.Show()

    @classmethod
    def BuildJobMenu( pyClass, menuRef, imageLibrary, cityName, playerRef, options ):
        Logger.Write( "Create menu", "Menus::BuildJobMenu", "#00ff00" )
        pyClass.ClearMenu( menuRef )
        menuRef.SetName( "JobMenu" )

        screenWidth = int(ConfigManager.options[ "screenwidth" ])
        screenHeight = int(ConfigManager.options[ "screenheight" ])
        width = 320
        height = 450

        menuLeft = pyClass.menuX
        menuTop = 100
        menuRight = menuLeft + width
        menuBottom = menuTop + height

        buttony = 75
        inc = 75

        menuRef.SetBackground( { "color" : pygame.Color( 0, 0, 50 ), "x" : menuLeft, "y" : menuTop, "width" : width, "height" : height } )

        menuRef.AddLabel( "header", {
            "label_font" : FontManager.Get( "main" ),
            "label_text" : "Gig Economy",
            "label_position" : ( menuLeft + 10, menuTop + 10 ),
            "label_color" : pygame.Color( 255, 255, 255 )
            } )


        menuRef.AddButton( "job-deliver", {
            "button_texture" : imageLibrary["buttonbg_ingame"],
            "button_position" : ( menuLeft + 10, menuTop + buttony ),
            "button_type" : "confirm",
            "label_font" : FontManager.Get( "hud" ),
            "label_text" : "Deliver letter ($" + str( options["jobPay"]["letter"] ) + ")",
            "label_position" : ( 30, 10 ),
            "label_color" : pygame.Color( 255, 255, 255 )#, "label_secondary_color" : pygame.Color( 0, 0, 0 ), "label_style" : "outline"
            } )
            
        menuRef.AddButton( "job-food", {
            "button_texture" : imageLibrary["buttonbg_ingame"],
            "button_position" : ( menuLeft + 10, menuTop + buttony + ( inc * 1 ) ),
            "button_type" : "confirm",
            "label_font" : FontManager.Get( "hud" ),
            "label_text" : "Deliver fast food ($" + str( options["jobPay"]["food"] ) + ")",
            "label_position" : ( 30, 10 ),
            "label_color" : pygame.Color( 255, 255, 255 )#, "label_secondary_color" : pygame.Color( 0, 0, 0 ), "label_style" : "outline"
            } )
            
        menuRef.AddButton( "job-vroomr", {
            "button_texture" : imageLibrary["buttonbg_ingame"],
            "button_position" : ( menuLeft + 10, menuTop + buttony + ( inc * 2 ) ),
            "button_type" : "confirm",
            "label_font" : FontManager.Get( "hud" ),
            "label_text" : "Vroomr driver ($" + str( options["jobPay"]["vroomr"] ) + ")",
            "label_position" : ( 30, 10 ),
            "label_color" : pygame.Color( 255, 255, 255 )#, "label_secondary_color" : pygame.Color( 0, 0, 0 ), "label_style" : "outline"
            } )

        menuRef.AddButton( "leavecity", {
            "button_texture" : imageLibrary["buttonbg_ingame"],
            "button_position" : ( menuLeft + 10, menuTop + buttony + (inc * 6) ),
            "button_type" : "cancel",
            "label_font" : FontManager.Get( "hud" ),
            "label_text" : "Cancel",
            "label_position" : ( 30, 10 ),
            "label_color" : pygame.Color( 255, 255, 255 )#, "label_secondary_color" : pygame.Color( 0, 0, 0 ), "label_style" : "outline"
            } )
            
        menuRef.Show()


    @classmethod
    def BuildShopMenu( pyClass, menuRef, imageLibrary, cityName, playerRef, costs ):
        Logger.Write( "Create menu", "Menus::BuildShopMenu", "#00ff00" )
        pyClass.ClearMenu( menuRef )
        menuRef.SetName( "ShopMenu" )

        screenWidth = int(ConfigManager.options[ "screenwidth" ])
        screenHeight = int(ConfigManager.options[ "screenheight" ])
        width = 320
        height = 450

        menuLeft = pyClass.menuX
        menuTop = 100
        menuRight = menuLeft + width
        menuBottom = menuTop + height

        buttony = 75
        inc = 75

        menuRef.SetBackground( { "color" : pygame.Color( 50, 50, 0 ), "x" : menuLeft, "y" : menuTop, "width" : width, "height" : height } )

        menuRef.AddLabel( "header", {
            "label_font" : FontManager.Get( "main" ),
            "label_text" : "Consumerz",
            "label_position" : ( menuLeft + 10, menuTop + 10 ),
            "label_color" : pygame.Color( 255, 255, 255 )
            } )
            
        if ( "handgun" in costs ):
            menuRef.AddButton( "buy-weapon", {
                "button_texture" : imageLibrary[ "handgun" ],
                "button_position" : ( menuLeft + 10, menuTop + buttony ),
                "button_type" : "confirm",
                "label_font" : FontManager.Get( "hud" ),
                "label_text" : "Gun ($" + str( costs['handgun'] ) + ")",
                "label_position" : ( 80, 10 ),
                "label_color" : pygame.Color( 255, 255, 255 )#, "label_secondary_color" : pygame.Color( 0, 0, 0 ), "label_style" : "outline"
                } )

        menuRef.AddButton( "buy-ammo", {
            "button_texture" : imageLibrary["ammo"],
            "button_position" : ( menuLeft + 10, menuTop + 2*buttony ),
            "button_type" : "confirm",
            "label_font" : FontManager.Get( "hud" ),
            "label_text" : "Ammo ($" + str( costs['ammo'] ) + ")",
            "label_position" : ( 80, 10 ),
            "label_color" : pygame.Color( 255, 255, 255 )#, "label_secondary_color" : pygame.Color( 0, 0, 0 ), "label_style" : "outline"
            } )

        menuRef.AddButton( "leavecity", {
            "button_texture" : imageLibrary["buttonbg_ingame"],
            "button_position" : ( menuLeft + 10, menuTop + buttony + (inc * 6) ),
            "button_type" : "cancel",
            "label_font" : FontManager.Get( "hud" ),
            "label_text" : "Cancel",
            "label_position" : ( 30, 10 ),
            "label_color" : pygame.Color( 255, 255, 255 )#, "label_secondary_color" : pygame.Color( 0, 0, 0 ), "label_style" : "outline"
            } )
            
        menuRef.Show()

    @classmethod
    def ClearMenu( pyClass, menuRef ):
        menuRef.Clear()
        menuRef = Menu()
