#!/usr/bin/python
# -*- coding: utf-8 -*-

import pygame, sys
from pygame.locals import *

from ezha import BaseState
from ezha import FontManager
from ezha import LanguageManager
from ezha import ConfigManager
from ezha import MenuManager

class TitleState( BaseState ):
    def __init__( self ):
        self.stateName = "titlestate"
        
    def Setup( self, screenWidth, screenHeight ):
        self.screenWidth = screenWidth
        self.screenHeight = screenHeight
        self.gotoState = ""

        MenuManager.Load( "assets/menus/titlescreen.menu" )
        
        
    def Update( self ):        
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
            
            if event.type == MOUSEBUTTONUP:
                mouseX, mouseY = event.pos

                clickAction = MenuManager.ButtonAction( mouseX, mouseY )
                
                if ( clickAction != "" ):
                    self.gotoState = clickAction
                
    
    def Draw( self, windowSurface ):
        MenuManager.Draw( windowSurface )

    
    def Clear( self ):
        MenuManager.Clear()
