#!/usr/bin/python
# -*- coding: utf-8 -*-

import pygame, sys
from pygame.locals import *

from ezha.inventory import InventoryItem

class Status( InventoryItem ):
    def __init__( self, options=None ):
        super( Status, self ).__init__()
        self.status = ""
        
        if ( options is not None ):
            self.Setup( options )
        
    def Setup( self, options ):
        super( Status, self ).Setup( options )
        self.status = options["status"]
        
    def Set( self, value ):
        self.status = value
        
    def Get( self ):
        return self.status
        
