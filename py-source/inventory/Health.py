#!/usr/bin/python
# -*- coding: utf-8 -*-

import pygame, sys
from pygame.locals import *

from ezha.inventory import InventoryItem

class Health( InventoryItem ):
    def __init__( self, options=None ):
        super( Health, self ).__init__()
        self.amount = 0
        
        if ( options is not None ):
            self.Setup( options )
        
    def Setup( self, options ):
        super( Health, self ).Setup( options )
        self.amount = options["amount"]
        
    def Add( self, amount ):
        self.amount += amount
        if ( self.amount > 100 ):
            self.amount = 100
        
    def Subtract( self, amount ):
        self.amount -= amount
        if ( self.amount < 0 ):
            self.amount = 0
    
    def GetAmount( self ):
        return self.amount
        
