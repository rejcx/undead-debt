#!/usr/bin/python
# -*- coding: utf-8 -*-

import pygame, sys
from pygame.locals import *

from ezha.inventory import InventoryItem

class Loan( InventoryItem ):
    def __init__( self, options=None ):
        super( Loan, self ).__init__()
        self.principle = 0
        self.interest = 0
        self.minimumPayment = 0
        self.minimumPaymentRate = 0
        self.lastTimeUpdated = 0
        self.lastCharge = 0
        
        if ( options is not None ):
            self.Setup( options )
        
    def Setup( self, options ):
        super( Loan, self ).Setup( options )
        self.name = options["name"]
        self.principle = options["principle"]
        self.interest = options["interest"]
        self.minimumPaymentRate = options["minimumPaymentRate"]
    
    def Update( self, timeCounter ):
        if ( timeCounter == self.lastTimeUpdated ):
            return None
        else:
            self.lastCharge = ( self.principle * self.interest )
            self.principle += self.lastCharge
            self.lastTimeUpdated = timeCounter
            return self.lastCharge
            
        
    def GetMinimumPayment( self ):
        return self.minimumPaymentRate * self.principle
        
    def GetMinimumPaymentString( self ):
        return '${:,.2f}'.format( self.GetMinimumPayment() )
        
    def GetLastChargeString( self ):
        return '${:,.2f}'.format( self.lastCharge )
        
    def GetPrinciple( self ):
        return self.principle
        
    def GetInterest( self ):
        return self.interest
        
    def GetPrincipleString( self ):
        return '${:,.2f}'.format( self.principle )
        
    def GetInterestString( self ):
        return str( self.interest * 100 ) + "%"
        
    def MakePayment( self, amount ):
        self.principle -= amount


