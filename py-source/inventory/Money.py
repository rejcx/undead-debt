#!/usr/bin/python
# -*- coding: utf-8 -*-

import pygame, sys
from pygame.locals import *

from ezha.inventory import InventoryItem

class Money( InventoryItem ):
    def __init__( self, options=None ):
        super( Money, self ).__init__()
        self.amount = 0
        
        if ( options is not None ):
            self.Setup( options )
        
    def Setup( self, options ):
        super( Money, self ).Setup( options )
        self.amount = options["amount"]
        
    def Add( self, amount ):
        self.amount += amount
        
    def Subtract( self, amount ):
        self.amount -= amount
    
    def GetAmount( self ):
        return self.amount
        
    def GetAmountString( self ):
        return '${:,.2f}'.format( self.amount )
        
