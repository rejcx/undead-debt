#!/usr/bin/python
# -*- coding: utf-8 -*-

import pygame, sys
from pygame.locals import *

from ezha.inventory import InventoryItem

class Gun( InventoryItem ):
    def __init__( self, options=None ):
        super( Gun, self ).__init__()
        self.bulletStyle = None
        self.reloadCooldown = 0
        self.reloadSpeed = 10
        self.ammo = 0
        self.ammoImage = None
        
        if ( options is not None ):
            self.Setup( options )
        
    def Setup( self, options ):
        super( Gun, self ).Setup( options )
        self.reloadSpeed = options["reloadSpeed"]
        self.bulletStyle = options["bulletStyle"]
        self.ammoImage = options["ammoImage"]
        
    def Update( self ):
        if ( self.reloadCooldown > 0 ):
            self.reloadCooldown -= 1
    
    def CanShoot( self ):
        return ( self.reloadCooldown <= 0 )
        
    def Shoot( self ):
        self.reloadCooldown = self.reloadSpeed
        
    def SetAmmo( self, amount ):
        self.ammo = amount
        
    def GetAmmo( self ):
        return self.ammo
        
    def AddAmmo( self, amount ):
        self.ammo += amount
        
    def SubtractAmmo( self, amount ):
        self.ammo -= amount
        
    def GetAmmoImage( self ):
        return self.ammoImage
