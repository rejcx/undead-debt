return {
  version = "1.1",
  luaversion = "5.1",
  tiledversion = "0.14.2",
  orientation = "orthogonal",
  renderorder = "right-down",
  width = 6,
  height = 8,
  tilewidth = 160,
  tileheight = 160,
  nextobjectid = 1,
  properties = {},
  tilesets = {
    {
      name = "tileset",
      firstgid = 1,
      tilewidth = 160,
      tileheight = 160,
      spacing = 0,
      margin = 0,
      image = "../../../../kingdom-math-board-game/design/tiled/tileset.png",
      imagewidth = 1920,
      imageheight = 2080,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 156,
      tiles = {}
    }
  },
  layers = {
    {
      type = "tilelayer",
      name = "Base",
      x = 0,
      y = 0,
      width = 6,
      height = 8,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "lua",
      data = {
        1, 1, 1, 1, 1, 1,
        98, 98, 98, 98, 98, 98,
        1, 1, 1, 1, 1, 1,
        23, 3, 2, 3, 3, 23,
        23, 2, 2, 2, 2, 23,
        23, 2, 3, 3, 2, 23,
        23, 2, 2, 2, 2, 23,
        3, 3, 3, 2, 3, 3
      }
    },
    {
      type = "tilelayer",
      name = "Decor",
      x = 0,
      y = 0,
      width = 6,
      height = 8,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "lua",
      data = {
        16, 17, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0,
        15, 16, 0, 15, 15, 15,
        0, 13, 0, 13, 13, 0,
        0, 0, 0, 0, 0, 0,
        0, 0, 14, 14, 0, 0,
        0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0
      }
    },
    {
      type = "tilelayer",
      name = "Paths",
      x = 0,
      y = 0,
      width = 6,
      height = 8,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "lua",
      data = {
        0, 0, 141, 128, 128, 128,
        0, 0, 4, 0, 0, 0,
        0, 0, 138, 0, 0, 0,
        0, 11, 54, 11, 11, 0,
        0, 69, 72, 56, 70, 0,
        0, 66, 11, 11, 66, 0,
        0, 81, 56, 71, 82, 0,
        11, 11, 12, 78, 10, 11
      }
    }
  }
}
