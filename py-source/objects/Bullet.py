#!/usr/bin/python
# -*- coding: utf-8 -*-

import pygame, sys
from pygame.locals import *
import math

from ezha.managers import FontManager
from ezha.ui import Label
from ezha import Logger

from ezha.objects import Character

class Bullet( Character ):
    def __init__( self ):
        #super().__init__()
        super( Bullet, self ).__init__()
        self.velX = 0
        self.velY = 0
        self.speed = 0
        self.angle = None
        self.image = None
        self.life = 100
        self.active = True
        
    def Setup( self, options ):
        #"playerx" : playerRect["x"],
        #"playery" : playerRect["y"],
        #"mousex" : self.mouseX,
        #"mousey" : self.mouseY,
        #"speed" : 2,
        #"image" : self.images["heart"]
        self.image = options["image"]
        self.speed = options["speed"]
        self.SetPosition( options["startx"], options["starty"] )
        self.angle = self.GetAngle( options["playerx"], options["playery"], options["mousex"], options["mousey"] )
        
    def GetAngle( self, x1, y1, x2, y2 ):
        angleRad = math.atan2( y2-y1, x2-x1 )
        angleDeg = math.degrees( angleRad )
        return angleRad
        
    def Update( self ):
        moveX = self.speed * math.cos( self.angle )
        moveY = self.speed * math.sin( self.angle )
        rect = self.GetRect()
        
        self.SetPosition( rect.x + moveX, rect.y + moveY )
        
        self.life -= 1
        if ( self.life <= 0 ):
            self.active = False
            
    def IsActive( self ):
        return self.active
        
    def Draw( self, window, camera=None ):
        posRect = self.posRect
        if ( camera is not None ):
            posRect = camera.GetAdjustedRect( self.posRect )
            
        if ( self.image is not None ):
            window.blit( self.image, posRect, self.frameRect )
