#!/usr/bin/python
# -*- coding: utf-8 -*-

import pygame, sys
from pygame.locals import *

import copy

from ezha.managers import FontManager
from ezha.ui import Label
from ezha import Logger
from ezha import Utilities

from ezha.objects import Character

class Enemy( Character ):
    def __init__( self ):
        super( Enemy, self ).__init__()
        self.goal = None
        self.damage = 5
        
    def GetDamage( self ):
        return self.damage
        
    def Update( self, playerRef ):
        super( Enemy, self ).Update()
        
        distance = Utilities.GetDistance( self.GetRect(), playerRef.GetRect() )
        
        if ( distance < 300 ):
            # Follow the player
            self.goal = copy.copy( playerRef.GetRect() )
            
        if ( self.goal is not None ):
            distanceToGoal = Utilities.GetDistance( self.GetRect(), self.goal )
            if ( distanceToGoal <= 10 ):
                self.ChangeAnimationAction( "idle-south" )
                
            else:                
                # Move horizontally if not within threshold
                if ( Utilities.Get1DDistance( self.posRect.x, self.goal.x ) > 10 ):
                
                    if ( self.posRect.x < self.goal.x ):
                        self.MoveXY( self.speed, 0 )
                        self.ChangeAnimationAction( "walk-east" )
                        
                    elif ( self.posRect.x > self.goal.x ):
                        self.MoveXY( -self.speed, 0 )
                        self.ChangeAnimationAction( "walk-west" )
                
                # Then move vertically
                else:
                    if ( self.posRect.y < self.goal.y ):
                        self.MoveXY( 0, self.speed )
                        self.ChangeAnimationAction( "walk-south" )
                        
                    elif ( self.posRect.y > self.goal.y ):
                        self.MoveXY( 0, -self.speed )
                        self.ChangeAnimationAction( "walk-north" )
                        
                    
