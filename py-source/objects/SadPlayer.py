#!/usr/bin/python
# -*- coding: utf-8 -*-

import pygame, sys
from pygame.locals import *

from ezha.managers import FontManager
from ezha.ui import Label
from ezha import Logger

from ezha.objects import Character

from inventory import Loan, Job, Gun, Money, Health, Status

class SadPlayer( Character ):
    def __init__( self ):
        #super().__init__()
        super( SadPlayer, self ).__init__()
        self.lastTimeUpdated = 0
        self.ammoCount = 0
        
        # Stats
        self.health = 100
        self.status = "Get a job"
        self.damageTimeout = 0
        
        # Inventory
        self.loans = {}
        self.loans["studentLoan"] = Loan( { "name" : "Student Loans", "principle" : 29497.25, "interest" : 0.0655, "minimumPaymentRate" : 0.01 } )
        self.money = Money( { "amount" : 1000 } )
        self.health = Health()
        self.status = Status( { "status" : "Get a job" } )
        self.guns = []
        self.currentGun = None
        self.job = None
        
        self.isMoving = False
    
        
    def Update( self, keys, levelMap, timeCounter, monthLength, effectManager, camera ):
        super( SadPlayer, self ).Update()
        if ( self.damageTimeout > 0 ):
            self.damageTimeout -= 1
            
        for key, loan in self.loans.iteritems():
            #if ( self.lastTimeUpdated != timeCounter ):
            if ( timeCounter != 0 and timeCounter % monthLength == 0 ):
                loan.Update( timeCounter )
                effectManager.PutFloatyTextAtPlayer( "Loans+", pygame.Color( 255, 0, 0 ), camera, self )
        
        if ( self.currentGun is not None ):
            self.currentGun.Update()

        self.isMoving = False
        movedX, movedY = self.HandleInput( keys, levelMap )
        
        if ( self.isMoving == False ):
            currentAnimation = self.GetAnimationAction()
            # Set animation icon
            if ( currentAnimation == "walk-north" or currentAnimation == "run-north"  ):
                self.ChangeAnimationAction( "idle-north" )
                
            elif ( currentAnimation == "walk-south" or currentAnimation == "run-south" ):
                self.ChangeAnimationAction( "idle-south" )
                
            elif ( currentAnimation == "walk-east" or currentAnimation == "run-east" ):
                self.ChangeAnimationAction( "idle-east" )
                
            elif ( currentAnimation == "walk-west" or currentAnimation == "run-west" ):
                self.ChangeAnimationAction( "idle-west" )

        return movedX, movedY
        
    def GetHurt( self, amount ):
        if ( self.damageTimeout <= 0 ):
            self.health.Subtract( amount )
            self.damageTimeout = 100
            return True
            
        return False

    def GetScreenPositionRect( self, camera ):
        posRect = self.posRect
        if ( camera is not None ):
            posRect = camera.GetAdjustedRect( self.posRect )
        
        return posRect

    def HandleInput( self, keys, levelMap=None ):
        animationType = ""
        if ( keys[ self.mappings[ "run" ] ] ):
            self.isRunning = True
            animationType = "run"
        else:
            self.isRunning = False
            animationType = "walk"

        vMove = ""
        hMove = ""
            
        if ( keys[ self.mappings[ "up" ] ] ):
            vMove = "UP"
            self.ChangeAnimationAction( animationType + "-north" )
            
        if ( keys[ self.mappings[ "down" ] ] ):
            vMove = "DOWN"
            self.ChangeAnimationAction( animationType + "-south" )
            
        if ( keys[ self.mappings[ "left" ] ] ):
            hMove = "LEFT"
            self.ChangeAnimationAction( animationType + "-west" )
            
        if ( keys[ self.mappings[ "right" ] ] ):
            hMove = "RIGHT"
            self.ChangeAnimationAction( animationType + "-east" )

        if ( hMove != "" or vMove != "" ):
            self.isMoving = True

            
        if ( self.allowDiagonal ):
            return self.Move( vMove, hMove, levelMap )
        else:
            return self.Move( hMove, "", levelMap )
            
