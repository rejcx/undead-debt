var bodyTypeMax = 2;
var bodyColorMax = 8;
var eyesMax = 6;
var mouthsMax = 9;
var glassesMax = 6;
var clothesTypeMax = 5;
var clothesColorMax = 2;
var clothesSizeMax = 2;
var facialTypeMax = 5;
var facialColorMax = 10;
var hairTypeMax = 9;
var hairBehindTypeMax = 7;
var hairColorMax = 12;

$( document ).ready( function() {
    $( ".options-header" ).click( function() {
        var sibs = $( this ).siblings( ".options-contents" );
        var icon = $( this ).children( ".icon" );

        if ( icon.eq(0).hasClass( "icon-collapse" ) )
        {
            // Collapse it
            icon.eq(0).removeClass( "icon-collapse" );
            icon.eq(0).addClass( "icon-expand" );
            sibs.eq(0).slideUp( "fast" );
        }
        else
        {
            // Expand it
            icon.eq(0).addClass( "icon-collapse" );
            icon.eq(0).removeClass( "icon-expand" );
            sibs.eq(0).slideDown( "fast" );
        }
    } );

    var bodySize = 1;

    function PopulateItems()
    {
        // Bodies
        for ( var c = 1; c <= bodyColorMax; c++ )
        {
            for ( var t = 1; t <= bodyTypeMax; t++ )
            {
                $( "#bodies" ).append( "<img src='images/bases/base" + t + "-c" + c + ".png' class='body-selection select-button' onclick='Change(\"base\", \"images/bases/base" + t + "-c" + c + ".png\")'>" );
            }
        }

        // Eyes
        for ( var t = 1; t <= eyesMax; t++ )
        {
            $( "#eyes" ).append( "<img src='images/eyes/eyes" + t + ".png' class='body-selection select-button' onclick='Change(\"eyes\", \"images/eyes/eyes" + t + ".png\")'>" );
        }

        // Mouths
        for ( var t = 1; t <= mouthsMax; t++ )
        {
            $( "#mouths" ).append( "<img src='images/mouths/mouth" + t + ".png' class='body-selection select-button' onclick='Change(\"mouth\", \"images/mouths/mouth" + t + ".png\")'>" );
        }

        // Glasses
        for ( var t = 1; t <= glassesMax; t++ )
        {
            $( "#glasses" ).append( "<img src='images/accessories/glasses" + t + ".png' class='body-selection select-button' onclick='Change(\"glasses\", \"images/accessories/glasses" + t + ".png\")'>" );
        }

        // Moustaches
        for ( var t = 1; t <= facialTypeMax; t++ )
        {
            for ( var c = 1; c <= facialColorMax; c++ )
            {
                $( "#hair-facial" ).append( "<img src='images/accessories/moustache" + t + "-c" + c + ".png' class='body-selection select-button' onclick='Change(\"hair-facial\", \"images/accessories/moustache" + t + "-c" + c + ".png\")'>" );
            }
        }

        // Clothes
        for ( var t = 1; t <= clothesTypeMax; t++ )
        {
            for ( var s = 1; s <= clothesSizeMax; s++ )
            {
                for ( var c = 1; c <= clothesColorMax; c++ )
                {
                    $( "#clothes" ).append( "<img src='images/clothes/clothes" + t + "-c" + c + "-size" + s + ".png' class='size" + s + " body-selection select-button' onclick='Change(\"clothes\", \"images/clothes/clothes" + t + "-c" + c + "-size" + s + ".png\")'>" );
                }
            }
        }

        // Hair
        for ( var t = 1; t <= hairTypeMax; t++ )
        {
            for ( var c = 1; c <= hairColorMax; c++ )
            {
                $( "#hair-top" ).append( "<img src='images/hair/hair" + t + "-c" + c + ".png' class='body-selection select-button' onclick='Change(\"hair-top\", \"images/hair/hair" + t + "-c" + c + ".png\")'>" );
            }
        }

        // Hair 2
        for ( var t = 1; t <= hairBehindTypeMax; t++ )
        {
            for ( var c = 1; c <= hairColorMax; c++ )
            {
                $( "#hair-behind" ).append( "<img src='images/hair/behindhair" + t + "-c" + c + ".png' class='body-selection select-button' onclick='Change(\"hair-behind\", \"images/hair/behindhair" + t + "-c" + c + ".png\")'>" );
            }
        }
    }

    function RandomLook()
    {
        var bodyType = Random( 1, bodyTypeMax );
        Change( "base", "images/bases/base" + bodyType + "-c" + Random( 1, bodyColorMax ) + ".png" );
        Change( "eyes", "images/eyes/eyes" + Random( 1, eyesMax ) + ".png" );
        Change( "mouth", "images/mouths/mouth" + Random( 1, mouthsMax ) + ".png" );
        Change( "clothes", "images/clothes/clothes" + Random( 1, clothesTypeMax ) + "-c" + Random( 1, clothesColorMax ) + "-size" + bodyType + ".png" );
        Change( "hair-top", "images/hair/hair" + Random( 1, hairTypeMax ) + "-c" + Random( 1, hairColorMax ) + ".png" );
    }

    $( "#random" ).click( function() {
        RandomLook();
    } );

    PopulateItems();
    RandomLook();
} );
    
function Random( min, max ) {
    return Math.floor( Math.random() * (max - min + 1) ) + min;
}

function Change( item, url )
{
    $( "#avatar-" + item ).attr( "src", url );
    $( "#avatar-" + item ).fadeIn( "fast" );

    if ( item == "base" )
    {
        if ( url.search( "base1" ) != -1 )
        {
            bodySize = 1;
            Change( "clothes", "images/clothes/clothes" + Random( 1, clothesTypeMax ) + "-c" + Random( 1, clothesColorMax ) + "-size" + bodySize + ".png" );
            $( ".size2" ).fadeOut( "fast" );
            $( ".size1" ).fadeIn( "fast" );
        }
        else
        {
            bodySize = 2;
            Change( "clothes", "images/clothes/clothes" + Random( 1, clothesTypeMax ) + "-c" + Random( 1, clothesColorMax ) + "-size" + bodySize + ".png" );
            $( ".size1" ).fadeOut( "fast" );
            $( ".size2" ).fadeIn( "fast" );
        }
        console.log( bodySize );
    }
}

function Clear( item )
{
    $( "#avatar-" + item ).fadeOut( "fast" );
}
